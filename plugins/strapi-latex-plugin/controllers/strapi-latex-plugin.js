'use strict';

/**
 * strapi-latex-plugin.js controller
 *
 * @description: A set of functions called "actions" of the `strapi-latex-plugin` plugin.
 */
module.exports = {


  async search(ctx){
       // Making an object with the attributes of each model
      const allModels = await strapi.plugins['strapi-latex-plugin']
        .services['strapi-latex-plugin'].getAllModels()

      // Querying the results based on the attributes of each model/type
      const results = await strapi.plugins['strapi-latex-plugin']
      .services['strapi-latex-plugin'].getResults(allModels)

      return results
  }
};