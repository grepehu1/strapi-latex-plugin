'use strict';

/**
 * strapi-latex-plugin.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

module.exports = {

    getAllModels: async () => {
        let models = strapi.models

        // Generic attributes and models that are not needed
        const unwantedAttributes = ['created_by', 'updated_by']
        const unwantedModels = ['core_store', 'strapi_webhooks']

        // Making an object with the attributes of each model
        const allModels = {}
        Object.keys(models)
            .filter(item => unwantedModels.indexOf(item) === -1)
            .map(key => {
                allModels[key] = {}
                allModels[key].modelType = models[key].modelType
                allModels[key].kind = models[key].kind
                const atts = models[key].attributes

                //Removing unwanted attributes
                unwantedAttributes.map(unw => {
                    delete atts[unw]
                })

                allModels[key].attributes = atts
            })

        return allModels
    },
    getResults: async (allModels) => {
        const types = Object.keys(allModels) || []
    
            // Querying the results based on the attributes of each model/type
            const results = {}
            for (let type of types){
                const singleResult = await strapi.query(type).find()
                const fieldsOfType = Object.keys(allModels[type].attributes) || []
        
                results[type] = []
                
                singleResult && singleResult.map(item => {
                    const itemObj = {}
                    fieldsOfType.map(field => {
                        itemObj[field] = item[field] || null
                    })
                    results[type].push(itemObj)
                })
            }
    
          return results
    }

};
